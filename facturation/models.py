from django.db import models

class Facture(models.Model):
    numero = models.CharField(unique=True, max_length=256)
    date = models.DateField()
    projet = models.ForeignKey('projets.Projet', on_delete=models.CASCADE)
    emise = models.BooleanField(verbose_name='Facturé')
    encaisse = models.BooleanField(verbose_name='Encaissé')
    nb_dj = models.IntegerField(verbose_name='Nombre de demi-journés facturés')
    frais = models.FloatField(verbose_name='Frais facturés', null=True)
    montant_facture = models.FloatField(verbose_name='Montant facturé')

    @property
    def get_taux_tva(self):
        tva = TVA.objects.filter(debut__lte=self.date, fin__gte=self.date)
        return tva.first().taux

    @property
    def montant_total(self):
        return (self.montant_ht * self.get_taux_tva)
    
    @property
    def montant_ht(self):
        return (self.frais + self.montant_facture)


class TVA(models.Model):
    taux = models.FloatField(null=False, help_text='ex pour 20% ecrire 0.2', verbose_name='Taux de TVA')
    debut = models.DateField(verbose_name='Date de debut de ce taux de TVA', null=False, blank=False)
    fin = models.DateField(verbose_name='Date de fin de ce taux de TVA', null=True, blank=True)


    