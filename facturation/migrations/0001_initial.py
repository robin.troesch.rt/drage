# Generated by Django 3.0.3 on 2020-03-22 21:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('projets', '0012_delete_intervention'),
    ]

    operations = [
        migrations.CreateModel(
            name='TVA',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('taux', models.FloatField(help_text='ex pour 20% ecrire 0.2', verbose_name='Taux de TVA')),
                ('debut', models.DateField(verbose_name='Date de debut de ce taux de TVA')),
                ('fin', models.DateField(blank=True, verbose_name='Date de fin de ce taux de TVA')),
            ],
        ),
        migrations.CreateModel(
            name='Facture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.CharField(max_length=256, unique=True)),
                ('date', models.DateField()),
                ('emise', models.BooleanField(verbose_name='Facturé')),
                ('encaisse', models.BooleanField(verbose_name='Encaissé')),
                ('nb_dj', models.IntegerField(verbose_name='Nombre de demi-journés facturés')),
                ('frais', models.FloatField(null=True, verbose_name='Frais facturés')),
                ('montant_facture', models.FloatField(verbose_name='Montant facturé')),
                ('projet', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projets.Projet')),
            ],
        ),
    ]
