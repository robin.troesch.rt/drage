from django.contrib import admin

from .models import TVA

class FacturationAdmin(admin.ModelAdmin):
    list_display = ('id','taux', 'debut', 'fin')

admin.site.register(TVA, FacturationAdmin)