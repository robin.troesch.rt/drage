from django import forms
from django.forms import modelformset_factory

from .models import *

class FactureProjetForm(forms.ModelForm):
    class Meta:
        model = Facture
        fields = ('numero', 'date', 'nb_dj', 'frais', 'montant_facture', 'encaisse')
        widgets= {
            'date': forms.DateInput(attrs={'type':'date'}),
            'encaisse': forms.CheckboxInput(attrs={'disabled':'disabled'})
        }

FactureFormSet = modelformset_factory(Facture,
    form=FactureProjetForm, extra=1, can_delete=True
)