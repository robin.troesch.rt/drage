from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    user = {
        'nom':'Robin',
    }
    context = {'user':user}
    if request.user.is_authenticated:
        return render(request,'index.html',context)
    else:
        return HttpResponse("Not logged in")
@login_required
def home(request):
    print(request.user.first_name)
    return render(request,'home.html')