from django import forms
from django.contrib.auth.models import User
from .models import *
from django.forms import modelformset_factory

class NewProjectForm(forms.ModelForm):
    class Meta:
        model = Projet
        fields = '__all__'
        exclude =  ('cloture',)
        help_texts = {'client': '<a href="/projets/client/new">Nouveau client ?</a>'} 


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Projet
        fields = '__all__'
        widgets= {
            'cloture': forms.DateInput(attrs={'type':'date'})
        }


class ClotureProjetForm(forms.ModelForm):
    class Meta:
        model = Projet
        fields = ['cloture']
        widgets= {
            'cloture': forms.DateInput(attrs={'type':'date'})
        }
class NewCommandeForm(forms.ModelForm):
    class Meta:
        model = Commande
        fields = '__all__'
        exclude = ['projet','numero']

MissionModelFormset = modelformset_factory(
    Mission,
    fields = '__all__',
    widgets={
            'commande': forms.HiddenInput()
        },
    extra=1,
    can_delete=True
)
class NewClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = '__all__'
class NewUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username','password','email','first_name','last_name']