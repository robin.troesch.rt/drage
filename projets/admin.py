from django.contrib import admin
from .models import Projet, Type_accompagnement, Plan_facturation

class ProjetAdmin(admin.ModelAdmin):
    list_display = ('id','nom')


class Type_accompagnementAdmin(admin.ModelAdmin):
    list_display = ('id', 'libelle')

class Plan_facturationAdmin(admin.ModelAdmin):
    list_display = ('id', 'libelle')

admin.site.register(Projet, ProjetAdmin)
admin.site.register(Type_accompagnement, Type_accompagnementAdmin)
admin.site.register(Plan_facturation, Plan_facturationAdmin)