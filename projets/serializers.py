from rest_framework import serializers
from .models import Projet,Client,Mission, Type_accompagnement
from django.contrib.auth.models import User

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

class ProjetSerializer(serializers.ModelSerializer):
    client = serializers.SlugRelatedField(slug_field="nom",read_only=True)
    accompagnement = serializers.SlugRelatedField(slug_field="libelle",read_only=True)
    plan_facturation = serializers.SlugRelatedField(slug_field="libelle",read_only=True)
    class Meta:
        model = Projet
        fields = '__all__'

class CollaborateurSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username','first_name','last_name','email']

class MissionSerializer(serializers.ModelSerializer):
    nom_projet = serializers.ReadOnlyField()
    to_staff = serializers.ReadOnlyField()
    numero_commande = serializers.ReadOnlyField()
    class Meta:
        model = Mission
        fields = ['id','montant','tjm','jours_vendus','nom_projet','to_staff','numero_commande']

class TypeAccompagnementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type_accompagnement
        fields = '__all__'