import ast

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.views.generic import UpdateView
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from .serializers import *
from .forms import *
from .models import *
from django.urls import reverse_lazy
from django.core.exceptions import ValidationError

from planning.models import Intervention
from facturation.models import Facture
from facturation.forms import FactureFormSet

  
class ProjetList(generics.ListAPIView):
    serializer_class = ProjetSerializer
    def get_queryset(self):
        acc = [acc.id for acc in Type_accompagnement.objects.all()]
        query_string = ''
        if self.request.GET.get('acc'):
            acc = ast.literal_eval(self.request.GET.get('acc'))
        if self.request.GET.get('string'):
            query_string = self.request.GET.get('string')
        if self.request.GET.get('cloture') and ast.literal_eval(self.request.GET.get('cloture')):
            projets = Projet.objects.filter(accompagnement__in=acc, nom__icontains=query_string, cloture__isnull=False)
        else:
            projets = Projet.objects.filter(accompagnement__in=acc, nom__icontains=query_string, cloture__isnull=True)
        return projets

class CollaborateurView(viewsets.ModelViewSet):
    serializer_class = CollaborateurSerializer
    queryset = User.objects.all()

class CollaborateurListCreate(generics.ListCreateAPIView):
    serializer_class = CollaborateurSerializer
    queryset = User.objects.all()



@api_view(['GET'])
def planCharge(request, id_projet):
    if request.method == 'GET':
        projet = Projet.objects.get(pk=id_projet)
        missions = Mission.objects.filter(commande__projet=projet)
        # Grouper les missions par id de collaborateur
        # Creer un dictionnaire avec 
        # {
        #    1: [{
        #    'jours_vendus':
        #    'id_mission':
        # }]
        # }
        payload = []
        collaborateurs = {}
        for mission in missions:
            data = {
                'jours_vendus': mission.jours_vendus,
                'tjm': mission.tjm,
                'id_mission': mission.id
            }
            # Groupe toutes les missions d'un collaborateur
            current_key = mission.collaborateur.id
            collaborateurs.setdefault(current_key, []).append(data)
        # Reprend tous les collaborateurs
        # Cree le paquet de donnees associees
        for id_collab in collaborateurs:
            collaborateur = User.objects.get(pk=id_collab)
            payload_collab = {
                'firstName': collaborateur.first_name,
                'lastName': collaborateur.last_name,
                'id': collaborateur.id
            }
            payload_mission = []
            # Calcul du planifie et total global
            total = 0
            planifie = 0
            payload_interventions = []
            # Parcours l'agregat de missions pour creer le paquet de donnees
            for mission in collaborateurs[id_collab]:
                nb_interventions = 0
                # Recupere les interventions, le nombre de jours places et le total / mission
                if mission['jours_vendus'] is not None:
                    interventions = Intervention.objects.filter(mission=mission['id_mission'])
                    # Augmente le total et le planifie global si des jours ont ete vendus
                    total+= mission['jours_vendus']
                    planifie+=len(interventions)
                    for intervention in interventions:
                        payload_interventions.append({
                            'date': intervention.date,
                            'am': intervention.am,
                            'lieu': projet.ville,
                            'projet': projet.nom,
                            'prix': mission['tjm'] * 0.5
                        })
                    # Recupere les interventions
                    
                payload_mission.append({
                    'id_mission': mission['id_mission'],
                    'jours_vendus': mission['jours_vendus'],
                    'planifies': nb_interventions
                })
            payload_interventions.sort(key=lambda x: x['date'])
            payload.append({
                'collaborateur':payload_collab,
                'missions': payload_mission,
                'meta': {
                    'planifie':planifie,
                    'total':total
                },
                'interventions': payload_interventions
            })
        return Response(payload)

@api_view(['GET'])
def accompagnements(request):
    if request.method == 'GET':
        type_acc = Type_accompagnement.objects.all()
        serializer = TypeAccompagnementSerializer(type_acc,  many=True)
        return Response(data=serializer.data)

def admin(request):
    form_new_client = NewClientForm(request.POST or None)
    form_new_user = NewUserForm(request.POST or None)
    return render(request,'admin.html',locals())

def new_client_admin(request):
    form_new_client = NewClientForm(request.POST or None)
    form_new_user = NewUserForm(None)
    if request.method == 'POST':
        form_new_client = NewClientForm(request.POST)
        if form_new_client.is_valid():
            form_new_client.save()
    return render(request,'admin.html',locals())

def new_user_admin(request):
    form_new_client = NewClientForm(None)
    form_new_user = NewUserForm(request.POST or None)
    if form_new_user.is_valid():
        sign_up = form_new_user.save(commit=False)
        sign_up.password = make_password(form_new_user.cleaned_data['password']) 
        sign_up.save()
    return render(request,'admin.html',locals())
def activeProjectsView(request):
    return render(request,'active_projects.html')
def archivedProjectsView(request):
    return render(request, 'archived_projects.html')
def newClient(request):
    form = NewClientForm(request.POST or None)
    if form.is_valid():
        form.save()
    return render(request,'new_client.html',locals())

def newProject(request):
    form = NewProjectForm(request.POST or None)
    if form.is_valid():
        form.save()
    return render(request,'new_project.html',locals())

def editProject(request,id_projet):
    """
    Permet d'afficher la page d'édition d'un projet
    """
    # Récupère le formulaire pour créer une commande
    newCommande = NewCommandeForm()
    # Récupère les données d'un projet et ses commandes
    projet = Projet.objects.get(id=id_projet)
    commandes = Commande.objects.filter(projet=id_projet)
    factures = Facture.objects.filter(projet=projet)
    factures_formset = FactureFormSet(queryset=factures)
    # Récupère les missions associées aux commandes
    commandes_missions = []
    dj_vendues = 0
    dont_hors_site = 0
    ca_vendu = 0
    for commande in commandes:
        montant_ht = 0
        missions = Mission.objects.filter(commande=commande.id)
        # Calcule le montant hors taxe de la commande
        for mission in missions:
            if mission.type_mission == '1' or mission.type_mission == '3':
                montant_ht += mission.montant
                ca_vendu += mission.montant
            elif mission.type_mission == '2':
                montant_ht += mission.jours_vendus * mission.tjm
                ca_vendu += mission.jours_vendus * mission.tjm
                dj_vendues += mission.jours_vendus
        missionsForm = MissionModelFormset(queryset=missions,
        initial=[{'commande':commande.id}])
        commandes_missions.append([commande,missionsForm, montant_ht])

    # Affiche le formulaire pour modifier le projet
    form = ProjectForm(request.POST or None, instance=projet)

    # Mets à jour le projet
    if request.method == 'POST':
        if form.is_valid():
          form.save()
    return render(request,'project.html',locals())

#Editer une commande, ajouter une mission a une commande
def editCommande(request,id_projet,id_commande):
    if request.method == 'POST':
        missionsForm = MissionModelFormset(request.POST)
        if missionsForm.is_valid():
            missionsForm.save()
    return redirect('projet:editProjet',id_projet=id_projet)
        
#Creer une nouvelle commande
def newCommande(request,id_projet):
    if request.method == 'POST':
        form = NewCommandeForm(request.POST or None)
        if form.is_valid():
            newCommande = form.save(commit=False)
            projet = Projet.objects.get(id=id_projet)
            nbCommandes = Commande.objects.filter(projet = id_projet).count()
            newCommande.numero = nbCommandes+1
            newCommande.projet = projet
            newCommande.save()
    return redirect('projet:editProjet',id_projet=id_projet)
# Vue pour cloturer le projet
class ClotureProjet(UpdateView):
    model = Projet
    form_class = ClotureProjetForm
    template_name = 'cloture.html'
    pk_url_kwarg = 'id_projet'

    def form_valid(self, form):
        form.save()
        return redirect('projet:active_projects')