from django.db import models
from django.apps import apps
from django.apps import apps
from django.contrib.auth.models import User
from django.conf import settings

from planning.models import Intervention

TYPES_MISSIONS = [
    ('1','Forfait'),
    ('2','Mission (planning)'),
    ('3','Frais vendus')
]
# Create your models here.
class Client(models.Model):
    nom = models.CharField(max_length=255)
    def __str__(self):
        return self.nom


class Type_accompagnement(models.Model):
    libelle = models.CharField(max_length=255)
    def __str__(self):
        return self.libelle


class Plan_facturation(models.Model):
    libelle = models.CharField(max_length=255)
    def __str__(self):
        return self.libelle


class Projet(models.Model):
    nom = models.CharField(max_length=255)
    ville=models.CharField(max_length=255)
    departement = models.IntegerField()
    cloture = models.DateField(null=True,blank=True)
    client = models.ForeignKey('Client',on_delete=models.CASCADE)
    accompagnement = models.ForeignKey('Type_accompagnement',on_delete=models.CASCADE)
    plan_facturation = models.ForeignKey('Plan_facturation',on_delete=models.CASCADE)
    km_AR = models.IntegerField(null=True,blank=True)
    peage = models.FloatField(null=True,blank=True)
    nb_AR = models.IntegerField(null=True,blank=True)
    autre = models.FloatField(null=True,blank=True)
    commentaire_frais = models.TextField(null=True,blank=True)
    
    def _str_(self):
        return self.nom
    @property
    def get_year(self):
        return self.cloture.year


class Commande(models.Model):
    libelle = models.CharField(max_length=255)
    projet = models.ForeignKey('Projet',on_delete=models.CASCADE)
    numero = models.IntegerField(default=1)
    def __str__(self):
        return self.libelle


class Collaborateur(models.Model):
    nom = models.CharField(max_length=255)
    prenom = models.CharField(max_length=255)


class Mission(models.Model):
    type_mission = models.CharField(choices=TYPES_MISSIONS, max_length=255, null=True)
    montant = models.IntegerField(null=True,blank=True)
    commande = models.ForeignKey(Commande, on_delete=models.CASCADE)
    collaborateur = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,null=True)
    tjm = models.IntegerField(null=True,blank=True)
    jours_vendus = models.IntegerField(null=True,blank=True)

    @property
    def nom_projet(self) -> str:
        projet = Projet.objects.get(pk=self.commande.projet.id)
        return projet.nom

    @property
    def to_staff(self)-> int:
        """
            Indique le nombre de dj restants a staffer pour la mission
        """
        interventions = Intervention.objects.filter(mission=self).count()
        return (self.jours_vendus - interventions)
    
    @property
    def numero_commande(self)-> int:
        """
        Indique le numero de la commande associee
        """
        return self.commande.numero