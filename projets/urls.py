from django.urls import path, include
from rest_framework import routers
from . import views

app_name = 'Projets'

router = routers.DefaultRouter()
router.register(r'collaborateurs', views.CollaborateurView, 'collaborateur')
urlpatterns = [
    path('admin/', views.admin, name='admin'),
    path('admin/client/new',views.new_client_admin,name='newClientAdmin'),
    path('admin/user/new',views.new_user_admin,name='newUserAdmin'),
    path('api/',include(router.urls)),
    path('api/projet/',views.ProjetList.as_view()),
    path('api/collaborateur',views.CollaborateurListCreate.as_view()),
    path('actives', views.activeProjectsView,name='active_projects'),
    path('new', views.newProject, name='newProject'),
    path('edit/<int:id_projet>',views.editProject,name='editProjet'),
    path('edit/<int:id_projet>/commandes/new',views.newCommande,name='newCommande'),
    path('edit/<int:id_projet>/commande/<int:id_commande>',views.editCommande,name='editCommande'),
    path('client/new', views.newClient,name='newClient'),
    path('api/projet/<int:id_projet>/plan_charge', views.planCharge, name='planCharge'),
    path('api/accompagnements', views.accompagnements, name='accompagnements'),
    path('archived', views.archivedProjectsView, name='archived_projects'),
    path('edit/<int:id_projet>/cloturer', views.ClotureProjet.as_view(), name='cloturer'),
]