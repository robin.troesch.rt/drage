## Repertoire du projet DRAGE ##

Ceci est le repertoire du projet DRAGE realise par Centrale Nantes Etudes
pour le compte de QUADRAGE Conseil.
Realisateur : Robin TROESCH

### Specs generales

Backend realise en Django 3.0 et Python 3.6+ pour sa simplicite d'utilisation 
et sa gestion des utilisateurs. La base de donnees utilisee est Postgres (cf docker compose)

Frontend :
    - la plupart des interfaces simples utilisent le systeme de
    templates de base fournies par django couplé à Bootstrap et Jquery
    - certaines fonctionalites avancées utilisent des composants React.
    Les composants React utilisent Axios pour se connecter a l'api du backend
    django. Pour la mise en forme Reactstrap est utilisé.
    Les composants sont attachés a des containers dans les templates django.

### Architecture du projet

```
├── drage (Repertoire general)
    ├── drage_app (Repertoire de configuration globale)
        ├── settings.py (Configuration globale de l'application)
        ├── url.py (Configuration des routes ou url globales)
        ├── wsgi.py (Application de production)
    ├── facturation (Application de facturation)
    ├── home    (Application d'accueil)
    ├── planning    (Application planning)
    ├── projet  (Application de gestion des projets structure equivalente)
        ├── templates   (Templates de l'appli projets)
        ├── migrations  (Migrations de db)
        ├── admin.py    (Vues pour l'interface admin de projets)
        ├── apps.py
        ├── forms.py    (Formulaires pour les projets)
        ├── models.py   (Models db pour les projets)
        ├── serializers.py  (Serializers pour l'api projets)
        ├── tests.py    (Tests unitaires)
        ├── url.py  (Urls de l'application projets)
        ├── views.py    (Vues de l'appli projets)
    ├── static  (Fichiers statiques)
        ├── assets  (Scripts & CSS pour les templates)
        ├── dist (Fichiers compiles pour les composants Reacts)
        ├── img (Images)
    ├── templates (Templates generales)
        ├── registration (Templates d'authentification)
        ├── base.html   (Template de base utilise par tous)
    ├── webapp (Fichiers sources des composants React)
    ├── .babelrc (Config du compilateur React)
    ├── docker-compose.yml (Lancement de la base de donnees)
    ├── manage.py (Fichier de lancement general de l'application)
    ├── package.json (Fichier de configuration de la webapp react)
    ├── README.md (Ce fichier)
    ├── requirements.txt (Requirements pour django)
    ├── webpack.config.js (Le fichier de configuration du compilateur React)
```

    
### Premier lancement
Toutes les commandes sont a faire a la racine du projet
0. Installer Python 3.6 et docker
1. Lancer la base de donnees. Pour cela executer docker-compose up -d
2. Installer les dependances react. Pour cela executer npm install
3. Compiler les composants React. Pour cela executer npm start
4. Recommandé : creer un environnement python virtuel pour le developpement
Pour cela installer venv (pip isntall venv). Puis executer python3 -m venv env
Pour activer l'environnement : source env/bin/activate
Pour le desactiver : deactivate
5. Installer les dependances python.
Pour cela executer pip3 install -r requirements.txt (a faire dans un virtual env)
6. Initialiser la BD pour cela lancer python3 manage.py migrate
7. Creer un superutilisateur (admin) python3 manage.py createsuperuser
8. Lancer l'application python3 manage.py runserver

### Passage en production
Le passge en production devra inclure plusieurs points a realiser :
- Creer un docker-compose de production avec ngnix et gunicorn
- Passer le mode debug a 0 dans la config django et specifier les allowed_host
- Utiliser des variables d'environnement pour ne pas ecrire les secrets en clair

### Utilisation en dev
Suivre les etapes du premier lancement.

Apres toute modification des models de django effectuer les migrations de la DB.
Faire :
- python3 manage.py makemigrations
- python3 manage.py migrate
Redemarrer le serveur django

Apres toute modification de la webapp react recompiler les sources avec npm start.
Pour recompiler a la volee faire npm start -- --watch
Cela permet de recompiler des que les fichiers sont modifies.

### Explications sur la webapp React

