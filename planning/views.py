from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from projets.models import Mission, Projet
from projets.serializers import MissionSerializer

from .models import Intervention
from .serializers import InterventionSerializer

def main(request):
    return render(request,'main.html',locals())

class GetMissionsToPlace(APIView):
    def get(self, request,id_collaborateur, format=None):
        queryset = Mission.objects.filter(commande__projet__cloture__isnull=True,
            collaborateur=id_collaborateur, type_mission='2', jours_vendus__isnull=False)
        filtered = [x for x in queryset if x.to_staff > 0]
        serializer = MissionSerializer(filtered, many=True)
        return Response(data=serializer.data)
class InterventionAPI(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated, ]
    def get(self, request, id_collaborateur):
        collaborateur = User.objects.get(pk=id_collaborateur)
        am = True if request.GET.get('am') == 'true' else False
        queryset = Intervention.objects.filter(date=request.GET.get('date'),
         mission__collaborateur=collaborateur, am=am)
        if queryset:
            serializer = InterventionSerializer(queryset.first())
            return Response(data=serializer.data,status=200)
        else:
            return Response(data=None,status=200)
    def post(self, request, id_collaborateur):
        newIntervention = Intervention()
        newIntervention.date = request.data['date']
        mission = Mission.objects.get(pk=request.data['mission'])
        newIntervention.mission=mission
        newIntervention.am = request.data['am']
        try:
            newIntervention.clean()
            newIntervention.save()
            serializer = InterventionSerializer(newIntervention)
            return Response(data=serializer.data,status=200)
        except ValidationError :
            return Response(data='Toutes les interventions ont deja ete palcees.', status=403)
    def delete(self, request, id_collaborateur, id_intervention):
        Intervention.objects.filter(id=id_intervention).delete()
        return Response(status=200)
