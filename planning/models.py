from django.db import models
from django.core.exceptions import ValidationError

class Intervention(models.Model):
    mission = models.ForeignKey('projets.Mission', on_delete=models.CASCADE)
    date = models.DateField()
    am = models.BooleanField()
    def clean(self):
        """
        Verifie que le nombre d'interventions ne depasse pas
        le nombre de jours vendus
        """
        nb_interventions = Intervention.objects.filter(mission=self.mission).count()
        if nb_interventions > self.mission.jours_vendus:
            raise ValidationError('Cette mission est deja pleine')
    @property
    def projet(self) -> str:
        """
        Retourne le nom du projet associe a l'intervention
        """
        projet = self.mission.commande.projet
        return projet.nom