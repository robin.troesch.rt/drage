from . import views
from django.urls import path, include

app_name = 'Planning'

urlpatterns = [
    path('', views.main, name='main'),
    path('api/collaborateur/<int:id_collaborateur>/intervention/new',views.InterventionAPI.as_view(),name='newMission'),
    path('api/collaborateur/<int:id_collaborateur>/intervention/<int:id_intervention>',views.InterventionAPI.as_view(),name='updateMission'),
    path('api/collaborateur/<int:id_collaborateur>/missions/toplan',views.GetMissionsToPlace.as_view(),name='getMissionsToPlace'),
    path('api/collaborateur/<int:id_collaborateur>/intervention/',views.InterventionAPI.as_view(),name='getInterventionByDate')
    ]