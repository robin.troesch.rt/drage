from rest_framework import serializers
from .models import Intervention


class InterventionSerializer(serializers.ModelSerializer):
    projet = serializers.ReadOnlyField()
    class Meta:
        model = Intervention
        fields = ['id','am','date','mission','projet',]