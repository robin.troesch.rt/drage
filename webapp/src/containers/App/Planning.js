import React from 'react';
import { Form, Button, ButtonGroup, ButtonToolbar,Input, FormGroup,Label, Row, Col } from 'reactstrap';
import Collaborateur from '../../components/Planning/collaborateur'
import axios from 'axios';
var moment = require('moment');
var moment = require('moment-business-days');
moment.updateLocale('fr', {
    workingWeekdays: [1, 2, 3, 4, 5],
    holidayFormat: 'DD-MM-YYYY'
 });

class Planning extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            possibleCollaborateurs : [],
            displayedCollaborateurs : [],
            selectedMonth : 0,
            months : [
                "Janv","Fév","Mars","Avr",
            "Mai","Juin","Juil","Aout","Sept",
            "Oct","Nov","Déc"
            ],
            allSelected:true,
            focus: null,
            dates:[],
            reload:true
        };
        this.handleSelectCollab = this.handleSelectCollab.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.reload = this.reload.bind(this);
        this.changeFocus = this.changeFocus.bind(this);
      }
    componentDidMount(){
        this.setState({focus:moment()});
        // Get all the possible users
        axios.get('/projets/api/collaborateurs')
        .then(res => {
            this.setState({possibleCollaborateurs: res.data});
            this.setState({displayedCollaborateurs: res.data.map((item, index) => index)});
        });
        this.reload();
    }
    reload(){
        this.setState({reload:false});
        setTimeout(function() {
            //Start the timer
            // Wait that state is ready before reloading
            this.setState({dates: get_next_7_business_days(this.state.focus)});
            this.setState({selectedMonth: this.state.focus.month()});
            this.setState({reload:true});
          }.bind(this), 100) 
    }
    handleSelectCollab(event){
        if (event.target.checked){
          this.setState({displayedCollaborateurs: this.state.displayedCollaborateurs.concat([event.target.value])})  
        }
        else{
          this.setState({displayedCollaborateurs: this.state.displayedCollaborateurs.filter((item, j) => event.target.value != item)});
        }
        this.reload()
      }
    handleSelectAll(event){
        if (event.target.checked){
            this.setState({allSelected:true});
          this.setState({displayedCollaborateurs: this.state.possibleCollaborateurs.map((item, index) => index)});
        }
        else{
            this.setState({allSelected:false});
          this.setState({displayedCollaborateurs: []});
        }
        this.reload();
    }
    changeFocus(event){
        if (event.target.value == '+'){
            this.setState({focus: this.state.focus.add(7, 'days')});
        }
        else if (event.target.value == '-'){
            this.setState({focus: this.state.focus.subtract(7, 'days')});
        }
        else{
            this.setState({focus: moment().set('month', event.target.value).startOf('month')})
        }
        this.reload()
    }
  render () {
    return (
        <div>
            <Form inline>
                <FormGroup className='mr-1'>
                    <Label><Input defaultChecked={true} onChange={this.handleSelectAll} type='checkbox'></Input>Tous</Label>
                </FormGroup>
                {this.state.possibleCollaborateurs.map((collab, index) =>
                    <FormGroup className='mx-1'>
                        <Label>
                            <Input defaultChecked={this.state.allSelected} onChange={this.handleSelectCollab} type='checkbox' value={index}></Input>
                            {collab.first_name} {collab.last_name}
                        </Label>
                    </FormGroup>
                )} 
            </Form>
            <Row>
                <ButtonToolbar>
                    <ButtonGroup>
                        {this.state.months.map((month, index) =>
                            <Button value={index} onClick={this.changeFocus} active={(index) === this.state.selectedMonth}>
                                {month}
                            </Button>
                            )}
                    </ButtonGroup>
                </ButtonToolbar>
                <Col xs='2'></Col>
                <Button color='secondary'>{this.state.focus && this.state.focus.year()}</Button>
            </Row>
            
            <br/>
            <Row>
                <Col xs='2'>
                </Col>
                <Col xs='10'>
                    <Row>
                        <Col xs='1'></Col>
                        <Col xs='11'>
                            <Row>
                                {this.state.dates.map(date=> <Col xs='1'>{moment(date).format('dd DD/MM')}</Col>)}
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
            {this.state.reload && this.state.displayedCollaborateurs.map(collab_index => 
                <Collaborateur dates={this.state.dates} collaborateur={this.state.possibleCollaborateurs[collab_index]}></Collaborateur>
                )}
            <br/>
            <Row>
                <Col xs='2'></Col>
                <Col xs='10'>
                    <Row>
                        <Col xs='2'>
                            <Button value='-' onClick={this.changeFocus}>- 7 jours</Button>
                        </Col>
                        <Col xs='8'></Col>
                        <Col xs='2'>
                            <Button value='+' onClick={this.changeFocus}>+7 jours</Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    )
  }
}
export default Planning;

function get_next_7_business_days(focus){
    // Find the monday
    let now = focus.day(1);
    let array = [0, 1, 2, 3, 4, 5, 6];
    return array.map((index, item)=> now.businessAdd(index).format('YYYY-MM-DD'))
}