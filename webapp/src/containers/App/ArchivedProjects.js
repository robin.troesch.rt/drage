import React from 'react';
import axios from 'axios';


import ProjectTile from '../../components/projectTile'
import { Row, Input, Label, Col } from 'reactstrap';

class ArchivedProjects extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            search_string: '',
            accompagnements: [],
            check_acc: [],
            render: false
        };
        this.searchByname = this.searchByname.bind(this);
        this.handleSelectAcc = this.handleSelectAcc.bind(this);
        this.handleSelectAll = this.handleSelectAll.bind(this);
        this.reload = this.reload.bind(this)
      }
    componentDidMount(){
        axios.get('/projets/api/accompagnements')
        .then(res => {
          this.setState({accompagnements:res.data});
          this.setState({check_acc: this.state.accompagnements.map(acc => acc.id)});
        });
        
        this.reload();
    }
    reload(){
      setTimeout(function() { //Start the timer
        // Wait that state is ready before reloading
        axios.get(`/projets/api/projet/?acc=[${this.state.check_acc}]&string=${this.state.search_string}&cloture=True`)
        .then(res => {
          this.setState({projects: res.data})
      });
        }.bind(this), 1000)
    }
    searchByname(event){
      if (event.target.value.length > 3){
        this.setState({search_string: event.target.value})
      }
      else{
        this.setState({search_string: ''})
      }
      this.reload();
    }
    handleSelectAcc(event){
      if (event.target.checked){
        this.setState({check_acc: this.state.check_acc.concat([event.target.value])})
        
      }
      else{
        this.setState({check_acc: this.state.check_acc.filter((item, j) => event.target.value != item)});
      }
      this.reload()
    }
    handleSelectAll(event){
      if (event.target.checked){
        this.setState({check_acc: this.state.accompagnements.map(acc => acc.id)});
      }
      else{
        this.setState({check_acc: []});
      }
      this.reload();
    }
  render () {
    return (
      <div>
        <Row>
          <Col xs='9'></Col>
          <Col xs='3'>
            <Input onChange={this.searchByname} type='text' placeholder='Rechercher par nom de projet'></Input>
          </Col>
        </Row>
        <Row>
            <Col xs='2'>
                <Label><Input defaultChecked={true} onChange={this.handleSelectAll} type='checkbox'></Input>Tous</Label>
            </Col>
          {this.state.accompagnements.map(acc =>
              <Col xs='2'>
                <Label><Input defaultChecked={true} onChange={this.handleSelectAcc} type='checkbox' value={acc.id}></Input>{acc.libelle}</Label>
              </Col>
            )} 
        </Row>
        <Row className='mt-3'>
            { this.state.projects.map(project => 
              <ProjectTile project={project} />
            ) }
        </Row>
      </div>
        
    )
  }
}
export default ArchivedProjects;