import React from 'react';
import { Card, Row, Col } from 'reactstrap';
import axios from 'axios';
var moment = require('moment');
var moment = require('moment-business-days');
moment.updateLocale('fr', {
    workingWeekdays: [1, 2, 3, 4, 5],
    holidayFormat: 'DD-MM-YYYY'
 });

class PlanCharge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            planCharge: []
        };

      }
    componentDidMount(){
        axios.get(`/projets/api/projet/${this.props.match.params.id}/plan_charge`)
        .then(res => {
            this.setState({planCharge: res.data});
        });
    }
  render () {
    return (
        <div>
                    <Row>
            {this.state.planCharge.map(charge => 
                <Col xs='12'>
                    <Row>
                        <Col xs='2'>
                            <Card className='text-center p-2 bg-light'>
                                <Row>
                                    <Col xs='12'>
                                    {charge.collaborateur.firstName} {charge.collaborateur.lastName}
                                    </Col>
                                    <Col xs='12'>
                                    {charge.meta.planifie}/{charge.meta.total} rdv
                                    </Col>
                                </Row>
                            </Card>
                        </Col>
                        <Col xs='10'>
                            <Row className='flex-row flex-nowrap' style={{overflowX: "scroll"}}>
                            {charge.interventions.map(intervention => 
                                <Col xs='3'>
                                    <Card>
                                        <Row className='small'>
                                            <Col xs='6'>
                                                {intervention.prix}€
                                            </Col>
                                            <Col xs='6'>
                                                {intervention.projet}
                                            </Col>
                                        </Row>
                                        {moment(intervention.date).format('dd DD/MM')}
                                        <Row className='small'>
                                            <Col>{intervention.lieu}</Col>
                                            <Col>{intervention.am ? 'AM' : 'PM'}</Col>
                                        </Row>
                                    </Card>
                                </Col>
                            )}
                            </Row>
                        </Col>
                    </Row>
                </Col>
            )}
        </Row>
        <br/>
        </div>
    )
  }
}
export default PlanCharge;