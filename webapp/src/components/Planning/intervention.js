import React from 'react';
import { Button, Col, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Card } from 'reactstrap';
import axios from 'axios';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

class Intervention extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            intervention: null,
            open_missions: [],
            dropdown_open: false
        };
        this.openDropdown = this.openDropdown.bind(this);
        this.placeIntervention = this.placeIntervention.bind(this);
        this.deleteIntervention = this.deleteIntervention.bind(this);
      }
    componentDidMount(){
      axios.get(`api/collaborateur/${this.props.collaborateur_id}/intervention/?date=${this.props.date}&am=${this.props.am}`)
      .then(res=>{
        this.setState({intervention:res.data})
      })
    }
    openDropdown(){
      axios.get(`api/collaborateur/${this.props.collaborateur_id}/missions/toplan`)
      .then(res => {
        this.setState({dropdown_open: !this.state.dropdown_open});
        console.log(res.data);
        this.setState({open_missions: res.data});
      }
        )
    }
    placeIntervention(event){
      let data = {
        'date':this.props.date,
        'mission':event.target.value,
        'am':this.props.am
      }
      axios.post(`api/collaborateur/${this.props.collaborateur_id}/intervention/new`, data)
      .then( res =>
        this.setState({intervention: res.data})
      )
    }
    deleteIntervention(){
      axios.delete(`api/collaborateur/${this.props.collaborateur_id}/intervention/${this.state.intervention.id}`)
      .then(res =>
        this.setState({intervention: null})
        )
    }
  render () {
    return (
            <Col xs='1'>
              {this.state.intervention ?
              <Card>
                {this.state.intervention.projet}
                <Button onClick={this.deleteIntervention} color='danger'>x</Button>
              </Card>:
              <ButtonDropdown heigth='20' isOpen={this.state.dropdown_open} toggle={this.openDropdown}>
                <DropdownToggle color='secondary'>
                  +
                </DropdownToggle>
                <DropdownMenu>
                  {this.state.open_missions.length > 0
                  ? <span>
                    {this.state.open_missions.map(mission=>
                      <DropdownItem value={mission.id} onClick={this.placeIntervention}>{mission.nom_projet} Commande n°{mission.numero_commande} ({mission.to_staff} dj restantes à staffer)</DropdownItem>
                    )}
                  </span>
                  : <p>Aucune mission a besoin de staffing</p>
                  }
                  
                </DropdownMenu>
              </ButtonDropdown>
              }
            </Col>
    )
  }
}
export default Intervention;