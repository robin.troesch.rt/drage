import React from 'react';
import {
    Card, CardBody, Col,
  } from 'reactstrap';

const ProjectTile = ({ project }) => {
    return (
        <Col xs='2'>
            <Card className='text-center'>
                <a href={`/projets/edit/${project.id}`}> 
                    <CardBody>
                        <p>{project.nom}</p>
                    </CardBody>
                </a>
            </Card>
        </Col>
    )
}
export default ProjectTile;