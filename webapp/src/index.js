import React from 'react';
import ReactDOM from 'react-dom';
import ActiveProjects from './containers/App/ActiveProjects';
import ArchivedProjects from './containers/App/ArchivedProjects';
import Planning from './containers/App/Planning';
import PlanCharge from './containers/App/PlanCharge';
import { BrowserRouter as Router, Route } from "react-router-dom";


ReactDOM.render(
  <Router>
    <Route exact path="/projets/actives">
        <ActiveProjects/>
      </Route>
      <Route exact path="/projets/archived">
        <ArchivedProjects/>
      </Route>
      <Route exact path="/planning/">
        <Planning/>
      </Route>
      <Route path="/projets/edit/:id" component={PlanCharge}>
      </Route>
  </Router>,
  document.getElementById('react-app')
);


