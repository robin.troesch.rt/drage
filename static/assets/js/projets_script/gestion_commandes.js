// Hide delete checkbox used by django to use only custom
$('input[name$="DELETE"]').parent().parent().hide()

function updateFields(el,value){
    switch (value){
        case '1':
            $(el).parent().parent().parent().find("input[name$='tjm']").parent().parent().hide();
            $(el).parent().parent().parent().find("input[name$='jours_vendus']").parent().parent().hide();
            $(el).parent().parent().parent().find("input[name$='montant']").parent().parent().show();
            break;
        case '2':
            $(el).parent().parent().parent().find("input[name$='jours_vendus']").parent().parent().show();
            $(el).parent().parent().parent().find("input[name$='tjm']").parent().parent().show();
            $(el).parent().parent().parent().find("input[name$='montant']").parent().parent().hide();
            break;
        case '3':
            $(el).parent().parent().parent().find("input[name$='tjm']").parent().parent().hide();
            $(el).parent().parent().parent().find("input[name$='jours_vendus']").parent().parent().hide();
            $(el).parent().parent().parent().find("input[name$='montant']").parent().parent().show();
            break;
        default:
    }
}
function updateElementIndex(el, prefix, ndx) {
    var id_regex = new RegExp('(' + prefix + '-\\d+)');
    var replacement = prefix + '-' + ndx;
    if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
    if (el.id) el.id = el.id.replace(id_regex, replacement);
    if (el.name) el.name = el.name.replace(id_regex, replacement);
}
function cloneMore(selector, prefix) {
    var newElement = $(selector).clone(true);
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    newElement.find(':input:not([type=button]):not([type=submit]):not([type=reset]):not(button)').each(function() {
        var name = $(this).attr('name').replace('-' + (total-1) + '-', '-' + total + '-');
        var id = 'id_' + name;
        
        $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
    });
    // Ajust the hidden input commande_id to match the commande_id of previous mission
    var commande_id = $(selector).find('input[name$="commande"]').val();
    $(newElement).find('input[name$="commande"]').val(commande_id);
    newElement.find('label').each(function() {
        var forValue = $(this).attr('for');
        if (forValue) {
          forValue = forValue.replace('-' + (total-1) + '-', '-' + total + '-');
          $(this).attr({'for': forValue});
        }
    });
    total++;
    $('#id_' + prefix + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
    var conditionRow = $('.form-row:not(:last)');
    conditionRow.find('.btn.add-form-row')
    .removeClass('btn-success').addClass('btn-danger')
    .removeClass('add-form-row').addClass('remove-form-row')
    .html('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
    return false;
}
function deleteForm(prefix, btn) {
    var total = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    if (total > 1){
        // Find the django checkbox for deletion and check it
        btn.parent().find('input[name$="DELETE"]').prop('checked',true);
        btn.closest('.form-row').hide();
        var forms = $('.form-row');
        $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
        for (var i=0, formCount=forms.length; i<formCount; i++) {
            $(forms.get(i)).find(':input').each(function() {
                updateElementIndex(this, prefix, i);
            });
        }
    }
    return false;
}
$(document).on('click', '.add-form-row', function(e){
    e.preventDefault();
    console.log($(this).parent().find('.form-row:last'));
    cloneMore($(this).parent().find('.form-row:last'), 'form');
    return false;
});
$(document).on('click', '.remove-form-row', function(e){
    e.preventDefault();
    deleteForm('form', $(this));
    return false;
});
// Update the fields of a mission on select
$('select[name$="type_mission"]').on('change',function(){
    updateFields($(this),$(this).val());
});

// Control the new commande view
$('#createCommandeDiv').hide();
$('#createCommandeBtn').on('click',function(){
    $('#createCommandeBtn').hide();
    $('#createCommandeDiv').show();
});
$('#cancelCreateCommande').on('click',()=>{
    $('#createCommandeBtn').show();
    $('#createCommandeDiv form').trigger('reset');
    $('#createCommandeDiv').hide();
});
